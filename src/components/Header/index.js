import React from 'react';

import './styles.css';

const Header = () => (
    <header id="main-header">We Love Pets</header>
);

export default Header;